# Convrt analytics package for fetching front-end events


### 1. Using it in Vanillia JS
```
<script src="https://convrt.io/backend_api/static/events-v`<script_version>`.min.js"></script>

<script>
    new ConvrtPixel('<client_id>').PixelInit();
</script>
```

## 1.1. Using it on Nextjs Typescript
```
...
declare class ConvrtPixel {
  constructor(clientId: string)
  PixelInit() : void
}

const MyApp = ({ Component, pageProps }: AppProps) => {
    ...
    return (
        <>
            <Script src="https://convrt.io/backend_api/static/events-v`<script_version>`.min.js"
                onLoad={() => {
                    new ConvrtPixel('<client_id>').PixelInit()
                }}
            />
        ...
        </>
    );
};

export default MyApp;
```

### 2. Using it as a node module


Install node module

```

npm i @convrt.io/events

```

Then in your code:

```
import { ConvrtPixel } from '@convrt.io/events';
```
And you need to run below method in componentDidMound lifecycle of App.
```
new ConvrtPixel('<client_id>').PixelInit();
```

### 3. Examples
Using Vanillia JS Example
```
<body>
...
...

<script src="https://convrt.io/backend_api/static/events-v1.0.0.min.js"></script>
<script>
    new ConvrtPixel('<client_id>').PixelInit();
</script>
</body>
```

React example
```
import { useEffect } from 'react';
import { ConvrtPixel } from '@convrt.io/events';
...

function App() {
    ...
    useEffect(() => {
        new ConvrtPixel('<client_id>').PixelInit();
    }, []);

    ...
}


If you have multi API calling error, please check this link: https://stackoverflow.com/a/72238236/19871084
```

Vue example
```
import { ConvrtPixel } from '@convrt.io/events';

...
...

onMounted(() => {
    ...
    new ConvrtPixel('<client_id>').PixelInit();
    ...
});
```

## We are maintaining this module from version 1.0.0